"use strict";
var cards = Array.from(document.querySelectorAll('.card'));
//раскладываем карты в случайном порядке
function createRandomCards() {   
   var cardsRandomArr = [];
   var content = ['🐟', '🐟', '🐙', '🐙', '🦀', '🦀', '🐷 ', '🐷 ', '🐼', '🐼', '🐱', '🐱'];

   cards.forEach(function(elem) {
       var index = Math.floor(Math.random() * (content.length - 1));
       cardsRandomArr.push(newCard(elem, content[index]));
       elem.setAttribute('value',content[index]);       
       elem.classList.add('firstCard'); //добавляем флаг первой карты
       content.splice(index, 1);
   });

   function newCard(elem, content) {
       var children = elem.childNodes;
       children.forEach(function(child) {
           if ('classList' in child) {
                 if (child.classList.contains('back')) 
                    child.innerHTML = content;               
            }
        });
    }    
    return cards;
}

//запускаем игру
function gameStart() { 
    var container = document.querySelector('.gameContainer');  
    container.addEventListener('click', function(event) {
        var clicked = event.target.parentNode;        
        if (clicked.classList.contains('card')) {
            //если у карточки есть класс 'firstCard', то запускаем таймер
            if (clicked.classList.contains('firstCard')) timerStart();
            //если у карточки есть класс 'blocked', то ничего не делаем
            if (clicked.classList.contains('blocked')) return;
            //переворачиваем карточку
            clicked.classList.toggle('rotated');
            //запускаем функцию поиска совпадений
            tryIt();                   
        }
    });
}

function tryIt() {  
   // если находим карты с классом ошибки, переворачиваем их обратно
   var mistakenCards = Array.from(document.querySelectorAll('.thisIsFalse'));      
   if (mistakenCards !== undefined) {
        mistakenCards.forEach(function(card) {
            //удаляем классы ошибки и снимаем блокировку карты
       	    card.classList.remove('thisIsFalse');
            card.classList.remove('blocked');
            //удаляем цвет лицевой стороны
            removeClassFromBackSide(card,'wrongColor');
            //переворачиваем карты рубашкой вверх
            card.classList.remove('rotated');
       	    return card;
        });
   } 

   // находим открытые и не заблокированные карточки   
   var unblockedCards = [];   
   var rotatedCars = Array.from(document.querySelectorAll('.rotated'));

   rotatedCars.forEach(function(card) {       
       if (!card.classList.contains('blocked')) 
       return unblockedCards.push(card);   
   });

   // если открытая и незаблокированная карточка только одна, то ничего не делаем
   if (unblockedCards.length === 1) return;


   // проверяем совпадение
   if (unblockedCards.length === 2) {
   	   if (unblockedCards[0].getAttribute('value') === unblockedCards[1].getAttribute('value')) {
   	   	// если нашли совпадение, то добавляем класс совпадения 'thisIsRight' и 'blocked' (блокируем)
           unblockedCards.forEach(function (card) {
               card.classList.add('thisIsRight');
               card.classList.add('blocked');
               //меняем цвет лицевой стороны
               addClassToBackSide(card,'rightColor');           	   
           	   return card;
           });
        // если не совпало, то навесить класс ошибки 'thisIsFalse' и 'blocked' (блокируем до следубщего клика)
   	   } else {
   	   	   unblockedCards.forEach(function (card) {
               card.classList.add('thisIsFalse');
               card.classList.add('blocked');
               //меняем цвет лицевой стороны
               addClassToBackSide(card,'wrongColor');           	   
           	   return card;
   	        });
        }   
    }
}


//эти функции добавляют классы к лицевой стороне карты
function addClassToBackSide(card,cardClass) {
    var children = Array.from(card.childNodes);
    children.forEach(function(child) {
        if ('classList' in child) {
              if (child.classList.contains('back')) {
                  child.classList.add(cardClass);
            }
        }
    });
}
function removeClassFromBackSide(card,cardClass) {
    var children = Array.from(card.childNodes);
    children.forEach(function(child) {
        if ('classList' in child) {
              if (child.classList.contains('back')) {
                  child.classList.remove(cardClass);
            }
        }
    });
}

//логика игры
var timer = document.querySelector('.timer');

function timerStart() {
// удаляем флаг первой карточки
cards.forEach(function(elem) {
    elem.classList.remove('firstCard');
});
//Если всё поле открыто, а таймер не истёк, всплывает окно выигрыша
var rotatedCars = Array.from(document.querySelectorAll('.rotated'));

var time = timer.innerHTML;
var timeArr = time.split(":");
var minutes = timeArr[0];
var seconds = timeArr[1];
//таймер на 1 минуту
if (seconds == 0) {
      if (minutes == 0) {          
          showMessage();
          return;
        }
      minutes--;
      //таймер только на 1 минуту, поэтому строчку можно опустить
     // if (minutes < 10) minutes = "0" + minutes;
      seconds = 59;
    }
    else {
        if (rotatedCars.length === 12) {
            showMessage();
            return;
        }
        seconds--;
    }
    if (seconds < 10) seconds = "0" + seconds;
    document.querySelector('.timer').innerHTML = minutes + ":" + seconds;
    setTimeout(timerStart, 1000);
  }

// завершение и перезапуск игры
  var popUp = document.querySelector('.pop-up');
  var messageWin = document.querySelector('.text-win');
  var messageLose = document.querySelector('.text-lose');
  var button = document.querySelector('.button');

  function showMessage() {   
      var rotatedCars = Array.from(document.querySelectorAll('.rotated'));  
      popUp.classList.add('gameOver');
      if (rotatedCars.length === 12) {
           messageWin.classList.add('gameOver');
           button.innerHTML = 'Play again';
         } else {
           messageLose.classList.add('gameOver');
           button.innerHTML = 'Try again';
         }
  }

  
  button.addEventListener('click',function(event) {    
      event.preventDefault();
      popUp.classList.remove('gameOver');
      messageWin.classList.remove('gameOver');
      messageLose.classList.remove('gameOver');
      //перемешиваем карты
      createRandomCards();    
      //переворачиваем карты рубашкой вверх
      cards.forEach(function(elem) {
         elem.classList.remove('rotated');
         elem.classList.remove('blocked');
         elem.classList.remove('thisIsRight');
         elem.classList.remove('thisIsFalse');         
         return elem;
      });
      //устанавливаем значение таймера на 1 минуту
      timer.innerHTML = '01:00';
  });






